*********************************************************
*							*
*		MacFloppy.device			*
*							*
*	mfend.asm - defines ENDCODE			*
*							*
*	Copyright (c) 1989 Central Coast Software	*
*							*
*********************************************************

	XDEF	EndCode

	DC.W	0

EndCode	END
