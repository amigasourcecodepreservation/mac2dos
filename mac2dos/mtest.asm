*************************************************************************
*									*
*     Macintosh to AmigaDOS file conversion utility.			*
*									*
*	Copyright (c) 1988 Central Coast Software			*
*	    268 Bowie Dr, Los Osos, CA 93402				*
*	     All rights reserved, worldwide				*
*									*
*************************************************************************

;	INCLUDE "I/MACROS.ASM"
;	INCLUDE "I/EQUATES.ASM"
	INCLUDE	"I/MACMAC.ASM"

* HARDWARE REGISTER DEFINITIONS

DSKPTR	EQU	$DFF020
DSKLEN	EQU	$DFF024
DSKBYTR	EQU	$DFF01A
DSKSYNC	EQU	$DFF07E
CIAA	EQU	$BFE001
CIAB	EQU	$BFD100
INTENAR	EQU	$DFF01C
INTENA	EQU	$DFF09A
INTREQ	EQU	$DFF09C
INTREQR	EQU	$DFF01E
ADKCON	EQU	$DFF09E
ADKCONR	EQU	$DFF010
VHPOSR	EQU	$DFF006

TR_ADDREQUEST	EQU	9	;TIMER IO REQUEST

DF0		EQU	$08
NONE		EQU	DF0
CA0		EQU	$80+DF0
CA1		EQU	$01+DF0
CA2		EQU	$02+DF0
DrvEnable	EQU	$20+DF0
LSTRB		EQU	$10+DF0
SEL		EQU	$04+DF0

GO:
	MOVE.L  A7,initialSP		;initial task stack pointer
	MOVEA.L 4,A6
	MOVE.L  A6,SysBase
	LEA     DosName,A1
	ZAP	D0
	CALLSYS	OpenLibrary		;OPEN DOS LIBRARY
	MOVE.L  D0,LIBRARY_BASE
	ZAPA	A1
	CALLSYS	FindTask
	MOVE.L	D0,D2D_TASK		;save ptr for msg ports
	MOVEA.L D0,A4
COMMON:
	LEA	DISK_NAME,A1		;SET UP POINTER TO DISK RESOURCE
	CALLSYS	OpenResource
	MOVE.L  D0,DR_BASE		;POINTER TO DISK RESOURCE BLOCK
	BEQ	ABORT			;DIDN'T FIND IT
	LEA	CIAA,A2
	BSR	BUSY_DRIVES
;	MOVE.W	#0,DSKLEN		;enable reading
	BSR	MotorOn
	BSR	DriveRdy		;got a diskette?
	ERROR	9$
	BSR	WrtProt			;write protected?
	ERROR	9$
	BSR	HomeDisk
9$:	BSR	FREE_DRIVES
ABORT:	
	MOVE.L	initialSP,A7
	MOVE.L	SysBase,A6
	MOVE.L	LIBRARY_BASE,D0
	BEQ.S	1$
	MOVE.L	D0,A1
	CALLSYS	CloseLibrary
1$:	CLR.L	D0
	RTS			;BACK TO AMIGA-DOS


* SETS AMIGA-DOS DISK DRIVES BUSY FOR DURATION OF DISK OPERATION.

BUSY_DRIVES:
        PUSH    A0-A1
1$:     MOVE.L  DR_BASE,A6      ;ASK AMIGA-DOS POLITELY...
        LEA     DISK_BLOCK,A1
        MOVE.L  D2D_TASK,MP_TASK
        CALL    DR_GetUnit(A6)  ;...FOR THE DISK CONTROLLER
        TST.L   D0
        BNE     2$              ;GOT THE DISK
        LEA     MSG_PORT,A0
        CALLSYS	WaitPort,SysBase ;WILL SEND REPLY TO DRU PORT
        LEA     MSG_PORT,A0
        CALLSYS	GetMsg		;UNQUEUE REPLY
        BRA.S   1$              ;NOW TRY FOR IT AGAIN
2$:     POP     A0-A1
        RTS

* FREES AMIGA-DOS DISK DRIVES AFTER DISK OPERATION.

FREE_DRIVES:
        MOVE.L  DR_BASE,A6 
        CALL    DR_GiveUnit(A6) ;FREE THE DISK RESOURCE
        RTS 

* Turns MAC drive motor on using Apple procedures.

MotorOn:
	MOVE.B	#$FF,CIAB		;all to 0 (except DF0)
	MOVE.B	#CA0!CA1,CIAB
	MOVE.B	#CA1,CIAB
	MOVE.B	#CA1!CA2,CIAB
	MOVE.B	#CA1!CA2!LSTRB,CIAB
	NOP
	NOP
	MOVE.B	#CA0!CA1,CIAB
	RTS

* Checks for disk in place.

DriveRdy:
	MOVE.B	#SEL,CIAB		;CSTIN=disk in place
	MOVE.B	(A2),D0
	AND.W	#$20,D0
	BNE.S	9$
	STC
9$:	RTS

* Checks installed disk for write protection set.

WrtProt:
	MOVE.B	#SEL!CA0,CIAB		;WRTPRT=write locked
	MOVE.B	(A2),D0
	AND.W	#$20,D0
	BEQ.S	9$			;write enabled
	STC				;else error
9$:	RTS

* Moves head to track 0.

HomeDisk:
	BSR.S	Track0
	NOERROR	9$
	CLR.B	DIR			;first step away from track 0
	BSR.S	SetDirection
	MOVEQ	#15,D2
2$:	BSR.S	STEP
	DBF	D2,2$			;loop
	MOVE.B	#$20,DIR		;direction=toward TRACK 0
	BSR.S	SetDirection
1$:	BSR.S	Track0
	NOERROR	9$
	BSR	STEP
	BRA.S	1$
9$:	RTS

* Checks for head over track 0.

Track0:
	MOVE.B	#SEL!CA1,CIAB		;TK0
	MOVE.B	(A2),D0
	AND.W	#$20,D0
	BNE.S	9$			;at track 0
	STC				;else error
9$:	RTS

* STEPS head in preset direction, then waits for step flag to drop.

STEP:
	CLR.L	COUNT
	MOVE.B	#CA0!CA1,CIAB		;now issue step command
	MOVE.B	#CA0,CIAB
	MOVE.B	#CA0!LSTRB,CIAB
	NOP
	NOP
	NOP
	NOP
	MOVE.B	#CA0,CIAB
1$:	MOVE.B	(A2),D0
	AND.W	#$20,D0
	BEQ.S	2$
	ADDQ.L	#1,COUNT
	BRA.S	1$
2$:	RTS

* Sets stepping direction according to DIR: 0=away from trk 0, 1=toward 0.

SetDirection:
	MOVE.B	#CA0!CA1,CIAB
	TST.B	DIR			;toward track 0?
	BEQ.S	1$			;no...away
	MOVE.B	#CA2,CIAB
	MOVE.B	#CA2!LSTRB,CIAB		;toward 0
	NOP
	NOP
	MOVE.B	#CA2,CIAB
	BRA.S	2$
1$:	MOVE.B	#LSTRB,CIAB		;away from 0
	NOP
	NOP
;	MOVE.B	#NONE,CIAB
2$:	MOVE.B	#CA0!CA1,CIAB
	RTS

Eject:
	MOVE.B	#CA0!CA1,CIAB
	MOVE.B	#CA0!CA1!CA2,CIAB
	MOVE.B	#CA0!CA1!CA2!LSTRB,CIAB
	NOP
	NOP
	MOVE.B	#CA0!CA1,CIAB
	RTS


DosName		DC.B	'dos.library',0
DISK_NAME	DC.B	'disk.resource',0

D2D_TASK	DC.L	0	;TASK POINTER
SysBase		DC.L	0
LIBRARY_BASE	DC.L	0

initialSP	DC.L	0
DR_BASE		DC.L	0	;DISK RESOURCE POINTER
COUNT		DC.L	0
DIR		DC.B	0	;step direction

* DISK RESOURCE UNIT STRUCTURE

INT_STRUCT MACRO
        NODE 2,0,0
        DC.L    0
        DC.L    0
        ENDM

        CNOP  0,4

DISK_BLOCK
        NODE    5,0,0
DB_PORT DC.L    MSG_PORT
        DC.W    DB_MLEN
        INT_STRUCT
        INT_STRUCT
        INT_STRUCT
DB_MLEN EQU *-DISK_BLOCK

TIMER_BLOCK
	NODE	5,0,0
TB_PORT	DC.L	MSG_PORT
	DC.W	TB_MLEN
	DC.L	0,0		;DEV, UNIT
TB_CMD	DC.W	0,0		;COMMAND, FLAGS, ERROR
TB_SECS	DC.L	0		;SECONDS
TB_USEC	DC.L	0		;MICROSECONDS
	DC.L	0,0,0,0		;STANDARD IO REQ EXT
TB_MLEN	EQU *-TIMER_BLOCK

MSG_PORT:
        NODE    4,0,0
        DC.B    0       ;FLAGS=SIGNAL
        DC.B    8       ;SIGNAL 8
MP_TASK DC.L    0       ;MY TASK
MP_HEAD DC.L    MP_TAIL ;HEAD
MP_TAIL DC.L    0       ;TAIL
        DC.L    MP_HEAD ;TAIL PRED
        DC.B    5       ;TYPE
        DC.B    0       ;PAD


        END


